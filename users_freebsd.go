package users

import (
	"bufio"
	"bytes"
	"io"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

type group struct {
	name      string
	usernames []string
}

// List returns a list of all users from the OS.
func List() ([]User, error) {
	groups, err := getGroups()
	if err != nil {
		return nil, err
	}
	file, err := os.Open("/etc/passwd")
	if err != nil {
		return nil, err
	}
	defer file.Close()
	users := make([]User, 0)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		user := readUser(scanner.Text(), groups)
		users = append(users, user)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return users, nil
}

// ListGroup returns all the users in the group specified by name from the OS.
func ListGroup(name string) ([]User, error) {
	// This is a poor implementation because it just asks for all users and then filters the list.
	// Maybe there is a better way in FreeBSD and this implementation can be optimized, but I could not find it.
	users := make([]User, 0)
	allUsers, err := List()
	if err != nil {
		return users, err
	}
	for _, user := range allUsers {
		for _, group := range user.Groups {
			if group == name {
				users = append(users, user)
				break
			}
		}
	}
	return users, nil
}

// AddUser adds the specified user to the OS.
func AddUser(username, password, fullname string) error {
	username = cleanString(username)
	username = strings.ReplaceAll(username, " ", "")
	username = strings.ToLower(username)
	fullname = cleanString(fullname)
	args := []string{"useradd", username, "-c", fullname, "-m", "-h", "0"}
	cmd := exec.Command("/usr/sbin/pw", args...)

	stdin, err := cmd.StdinPipe()
	if err != nil {
		return err
	}
	defer stdin.Close()

	stderr := bytes.Buffer{}
	cmd.Stderr = &stderr
	if err = cmd.Start(); err != nil {
		return err
	}
	io.WriteString(stdin, password)
	err = cmd.Wait()
	output := stderr.Bytes()
	if strings.Contains(string(output), "already exists") {
		return ErrUserExists
	}
	if err != nil {
		return err
	}
	return nil
}

// DeleteUser removes the user specified by name from the OS.
func DeleteUser(name string) error {
	err := exec.Command("/usr/sbin/pw", "userdel", name).Run()
	if err != nil {
		return err
	}
	return nil
}

// AddGroup adds the specified name as a new group to the OS. Returns ErrGroupExists if the group name already exists.
func AddGroup(name string) error {
	output, err := exec.Command("/usr/sbin/pw", "groupadd", name).CombinedOutput()
	if strings.Contains(string(output), "already exists") {
		return ErrGroupExists
	}
	if err != nil {
		return err
	}
	return nil
}

// DeleteGroup removes the group specified by name from the OS.
func DeleteGroup(name string) error {
	err := exec.Command("/usr/sbin/pw", "groupdel", name).Run()
	if err != nil {
		return err
	}
	return nil
}

func getGroups() (map[int]group, error) {
	groups := make(map[int]group)
	file, err := os.Open("/etc/group")
	if err != nil {
		return groups, err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		elements := strings.Split(line, ":")
		group := group{name: elements[0]}
		id, err := strconv.Atoi(elements[3])
		if err != nil {
			continue
		}
		groups[id] = group
		usernames := elements[4]
		if usernames != "" {
			for _, username := range strings.Split(usernames, ",") {
				group.usernames = append(group.usernames, username)
			}
		}
	}
	if err := scanner.Err(); err != nil {
		return groups, err
	}
	return groups, nil
}

func readUser(input string, groups map[int]group) User {
	elements := strings.Split(input, ":")
	user := User{
		ID:       elements[2],
		Name:     elements[0],
		FullName: elements[4],
		Home:     elements[5],
	}
	groupID, err := strconv.Atoi(elements[3])
	if err == nil {
		user.Groups = append(user.Groups, groups[groupID].name)
	}
	for _, group := range groups {
		for _, username := range group.usernames {
			if username == user.Name {
				user.Groups = append(user.Groups, group.name)
			}
		}
	}
	return user
}

func cleanString(input string) string {
	output := input
	output = strings.ReplaceAll(output, ",", "")
	output = strings.ReplaceAll(output, ":", "")
	return output
}
