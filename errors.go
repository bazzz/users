package users

import "errors"

var (
	// ErrGroupExists is returned when adding a group with a name that already exists in the OS.
	ErrGroupExists = errors.New("group already exists")

	// ErrUserExists is returned when adding a user with a name that already exists in the OS.
	ErrUserExists = errors.New("user already exists")
)
