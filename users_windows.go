package users

// List does nothing on Windows and returns an empty slice.
func List() ([]User, error) {
	return []User{}, nil
}

// ListGroup does nothing on Windows and returns List().
func ListGroup(name string) ([]User, error) {
	return List()
}

// AddUser does nothing on Windows and returns nil.
func AddUser(username, password, fullname string) error {
	return nil
}

// DeleteUser does nothing on Windows and returns nil.
func DeleteUser(name string) error {
	return nil
}

// AddGroup does nothing on Windows and returns nil.
func AddGroup(name string) error {
	return nil
}

// DeleteGroup does nothing on Windows and returns nil.
func DeleteGroup(name string) error {
	return nil
}
