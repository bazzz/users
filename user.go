package users

type User struct {
	ID       string
	Name     string
	FullName string
	Groups   []string
	Home     string
}
